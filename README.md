# World Knowledge

![](images/title.PNG)

## Description

**World Knowledge** is an application that allows users to view the details of every country in the world and find it's location on the map.

The app has two types of interactions:

- ## From list

You can select a country from a list and find it’s location on the Map. Details about the country will automatically appear on the screen. The details contain information about the country name, capital, languages, area and others. You can close the details by clicking on the info window.

![](images/country_list.PNG)

- ## From map

The second type of interaction is by opening the world map. You can select any place on the map and the application will provide information in which country is that location and also the details of the country. If the point you have chosen is not in any country, you will see the  message “ There is no country at this location” 

![](images/world_map.PNG) ![](images/country_details.PNG)

You also have the option to open the Wikipedia article for the specific country by pressing the “Find in Wikipedia” button. If no valid country is selected you will see the message  “No valid country is selected”

# Happy exploring the world!

## Trello:
https://trello.com/b/eJIx3JLu/worldknowledge


