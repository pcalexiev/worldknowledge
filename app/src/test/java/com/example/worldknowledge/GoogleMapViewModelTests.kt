package com.example.worldknowledge

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.example.worldknowledge.domain.Country
import com.example.worldknowledge.repositories.CountriesRepository
import com.example.worldknowledge.viewmodels.GoogleMapsViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.junit.MockitoJUnit


class GoogleMapViewModelTests {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    val repository: CountriesRepository = mock()
    val viewModel = GoogleMapsViewModel(repository)


//    @Test
//    fun changeCurrentCountry_Should_Return_Country_WhenLocationExists() {
//
//        //Arrange
//        val expectedCountry = Factory.createCountry()
//        val liveData = MutableLiveData<List<Country>>()
//        liveData.value = listOf(expectedCountry)
//
//        whenever(repository.countries).thenReturn(liveData)
//
//        //Act
//        viewModel.changeCurrentCountry(LatLng(Factory.POINT_IN_RANGE,Factory.POINT_IN_RANGE))
//        val returnedCountry: Country? = viewModel.selectedCountry.value
//
//        //Assert
//        Assert.assertSame(expectedCountry,returnedCountry)
//    }
//
//    @Test
//    fun changeCurrentCountry_Should_Return_Null_WhenLocationNotExists() {
//
//        //Arrange
//        val liveData = MutableLiveData<List<Country>>()
//        liveData.value = listOf(Factory.createCountry())
//
//        whenever(repository.countries).thenReturn(liveData)
//
//        //Act
//        viewModel.changeCurrentCountry(LatLng(Factory.POINT_OUT_OF_RANGE,Factory.POINT_IN_RANGE))
//        val returnedCountry: Country? = viewModel.selectedCountry.value
//
//        //Assert
//        Assert.assertNull(returnedCountry)
//    }

    @Test
    fun createCameraBoundsForCountry_Should_Return_AppropriateBounds(){

        //Arrange
        val country = Factory.createCountry()
        val expectedBounds = LatLngBounds(LatLng(country.south,country.west),LatLng(country.north,country.east))

        //Act
        val returnedBounds = viewModel.createCameraBoundsForCountry(country)

        //Assert
        Assert.assertEquals(expectedBounds,returnedBounds)
    }

    @Test
    fun updateCountry_Should_Update_ViewModelCountry(){

        //Arrange
        val expectedCountry = Factory.createCountry()

        //Act
        viewModel.updateCountry(expectedCountry)
        val returnedCountry = viewModel.selectedCountry.value

        //Assert
        Assert.assertEquals(expectedCountry,returnedCountry)
    }

    @Test
    fun findCenterOfCountry_Should_Return_CenterOfCountryAsLocation(){

        //Arrange
        val country = Factory.createCountry()
        val expectedLatLng = LatLng(Factory.CENTER_OF_COUNTRY,Factory.CENTER_OF_COUNTRY)

        //Act
        val returnedLatLng = viewModel.findCenterOfCountry(country)

        //Assert
        Assert.assertEquals(expectedLatLng,returnedLatLng)
    }
}