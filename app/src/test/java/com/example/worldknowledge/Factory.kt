package com.example.worldknowledge

import com.example.worldknowledge.domain.Country

object Factory {
    const val COUNTRY_MIN_POINT = 0.0
    const val COUNTRY_MAX_POINT = 10.0
    const val CENTER_OF_COUNTRY = 5.0
    const val POINT_IN_RANGE = 5.0
    const val POINT_OUT_OF_RANGE = -5.0

    fun createCountry(): Country {
        return Country(
            0,
            "capital",
            "languages",
            0,
            0.0,
            "country name",
            "continent name",
            "currency code",
            "country code",
            east = COUNTRY_MAX_POINT,
            west = COUNTRY_MIN_POINT,
            north = COUNTRY_MAX_POINT,
            south = COUNTRY_MIN_POINT
        )
    }

    fun createListOfCountries(): List<Country> {
        return listOf(createCountry())
    }
}