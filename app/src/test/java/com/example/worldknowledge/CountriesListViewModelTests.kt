package com.example.worldknowledge

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.example.worldknowledge.domain.Country
import com.example.worldknowledge.repositories.CountriesRepository
import com.example.worldknowledge.viewmodels.CountriesListViewModel
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class CountriesListViewModelTests {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    val repository: CountriesRepository = mock()
    val viewModel = CountriesListViewModel(repository)

    @Test
    fun displayCountry_Should_Provide_TheCorrectCountry(){

        //Arrange
        val expectedCountry = Factory.createCountry()

        //Act
        viewModel.displayCountry(expectedCountry)

        //Assert
        Assert.assertSame(viewModel.navigateToSelectedCountry.value,expectedCountry)
    }

    @Test
    fun displayCountryComplete_Should_ChangeCountryToNull(){
        //Act
        viewModel.displayCountryComplete()

        //Assert
        Assert.assertNull(viewModel.navigateToSelectedCountry.value)
    }
}