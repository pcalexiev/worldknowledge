package com.example.worldknowledge.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Country(
    val id: Int,
    val capital: String,
    val languages: String,
    val population: Long,
    val areaInSqKm: Double,
    val countryName: String,
    val continentName: String,
    val currencyCode: String,
    val countryCode: String,
    var east: Double,
    var west: Double,
    var north: Double,
    var south: Double
) : Parcelable