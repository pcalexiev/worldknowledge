package com.example.worldknowledge


object Utils {
    fun formatLanguages(languages: String): String {
        return languages.replace(",", ", ")
    }

    fun formatPopulation(population: Long): String {
        val numberOfSpaces = population.toString().length / 3
        var populationString = population.toString()

        var numberOfCycles = 0
        for (i in 1 until numberOfSpaces + 1) {

            var positionOfSpace = populationString.length - (i * 3) - numberOfCycles

            populationString =
                populationString.substring(0, positionOfSpace) + " " + populationString.substring(positionOfSpace)

            numberOfCycles++
        }

        return populationString
    }

    fun formatArea(area: Double): String{
        val stringArea = area.toString()

        val afterDecimalSign = stringArea.substring(stringArea.indexOf('.'))

        val formattedArea = formatPopulation(area.toLong())

        return formattedArea + afterDecimalSign + " km\u00B2"
    }
}