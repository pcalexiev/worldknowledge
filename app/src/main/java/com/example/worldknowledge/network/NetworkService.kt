package com.example.worldknowledge.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import javax.annotation.Generated


private const val BASE_URL = "http://api.geonames.org/"

//private const val CREDENTIALS = "&username=petar_aleksiev"
private const val CREDENTIALS = "petar_aleksiev"

/*
All web services can be found here:
http://www.geonames.org/export/ws-overview.html
*/

interface GeoNamesService {
    @GET("countryInfoJSON")
    fun getCountries(@Query("username") credentials: String = CREDENTIALS): Deferred<NetworkCountriesContainer>

    @GET("countryCode")
    fun getCountryCodeFromLocation(
        @Query("lat") latitude: Double,
        @Query("lng") longitude: Double,
        @Query("username") credentials: String = CREDENTIALS
    ): Deferred<String>
}

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

object Network {

    val geoNamesService: GeoNamesService by lazy {
        retrofit.create(GeoNamesService::class.java)
    }
}