package com.example.worldknowledge.network


import com.example.worldknowledge.domain.Country
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class NetworkCountry(
    @Json(name = "geonameId") val id: Int,
    val capital: String,
    val languages: String,
    val population: Long,
    val areaInSqKm: Double,
    val countryName: String,
    val continentName: String,
    val currencyCode: String,
    val countryCode: String,
    val east : Double,
    val west : Double,
    val north : Double,
    val south : Double
)

@JsonClass(generateAdapter = true)
data class NetworkCountriesContainer(val geonames: List<NetworkCountry>)

fun NetworkCountriesContainer.asDomainModel(): List<Country>{
    return geonames.map {
        Country(
            id = it.id,
            capital = it.capital,
            languages = it.languages,
            population = it.population,
            areaInSqKm = it.areaInSqKm,
            countryName = it.countryName,
            continentName = it.continentName,
            currencyCode = it.currencyCode,
            countryCode = it.countryCode,
            east = it.east,
            west = it.west,
            north = it.north,
            south = it.south
        )
    }
}