package com.example.worldknowledge.viewmodels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.worldknowledge.domain.Country
import com.example.worldknowledge.repositories.CorrectionData
import com.example.worldknowledge.repositories.CountriesRepository
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker


class GoogleMapsViewModel @ViewModelInject constructor(
    private val countriesRepository: CountriesRepository
) : ViewModel() {

    private val _selectedCountry = MutableLiveData<Country>()
    val selectedCountry: LiveData<Country>
        get() = _selectedCountry

    private val _selectedCountryCode: MutableLiveData<String> =
        countriesRepository.selectedCountryCode
    val selectedCountryCode: LiveData<String>
        get() = _selectedCountryCode

    var requestCountryFromLocationCalled: Boolean = false

    fun createCameraBoundsForCountry(country: Country): LatLngBounds {
        return LatLngBounds(
            LatLng(country.south, country.west),
            LatLng(country.north, country.east)
        )
    }

    fun updateCountry(country: Country?) {
        _selectedCountry.value = country
    }

    fun findCenterOfCountry(country: Country): LatLng {

        if (CorrectionData.correctedCentersOfCountries.containsKey(country.countryName)) {
            return CorrectionData.correctedCentersOfCountries.get(country.countryName)!!
        }

        val latitude = (Math.max(country.north, country.south) - Math.min(
            country.north,
            country.south
        )) / 2 + Math.min(country.north, country.south)
        val longitude = (Math.max(country.east, country.west) - Math.min(
            country.east,
            country.west
        )) / 2 + Math.min(country.east, country.west)
        return LatLng(latitude, longitude)
    }

    fun correctZoom(
        cursorLatitude: Double,
        defaultZoom: Float
    ): Float {

        var newZoom = defaultZoom

        when (cursorLatitude.toInt()) {
            in 40..59 -> newZoom = 2.71f + ((cursorLatitude.toFloat() - 40) * 0.10f)
            in 60..80 -> newZoom = 2.91f + ((cursorLatitude.toFloat() - 60) * 0.10f)
            in 81..200 -> newZoom = 3.53f + ((cursorLatitude.toFloat() - 65) * 0.10f)
        }

        return newZoom
    }


    fun requestCountryFromLocation(position: LatLng) {
        countriesRepository.requestCountryCodeFromLocation(position.latitude, position.longitude)
    }

    fun findCountryFromCountryCode() {
        countriesRepository.countries.value?.let { countries ->
            _selectedCountry.value = null
            for (country in countries) {
                if (country.countryCode.equals(selectedCountryCode.value)) {
                    _selectedCountry.value = country
                }
            }
        }
    }

}