package com.example.worldknowledge.viewmodels


import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.worldknowledge.domain.Country
import com.example.worldknowledge.repositories.CountriesRepository


class CountriesListViewModel @ViewModelInject constructor(countriesRepository: CountriesRepository) : ViewModel() {

    private var _countries = MutableLiveData<List<Country>>()
    val countries: LiveData<List<Country>>
        get() = _countries

    private var _navigateToSelectedCountry = MutableLiveData<Country>()
    val navigateToSelectedCountry: LiveData<Country>
        get() = _navigateToSelectedCountry

    init {
        _countries = countriesRepository.countries
    }

    fun displayCountry(country: Country) {
        _navigateToSelectedCountry.value = country
    }

    fun displayCountryComplete(){
        _navigateToSelectedCountry.value = null
    }
}