package com.example.worldknowledge.viewmodels.adapters


import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.worldknowledge.domain.Country

@BindingAdapter("listOfCountries")
fun bindRecyclerViewToCountries(recyclerView: RecyclerView, data : List<Country>?){
    val adapter = recyclerView.adapter as CountryListAdapter
    adapter.addHeadersAndSubmitList(data)
}

@BindingAdapter("countryName")
fun TextView.bindCountryName(country: Country?){
    country?.let {
        text = country.countryName
    }
}

@BindingAdapter("headerName")
fun TextView.bindHeaderName(header: DataItem.Header?){
    header?.let {
        text = header.continentName
    }
}
