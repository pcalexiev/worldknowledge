package com.example.worldknowledge.viewmodels.adapters


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.worldknowledge.databinding.ListItemCountryBinding
import com.example.worldknowledge.databinding.ListItemHeaderBinding
import com.example.worldknowledge.domain.Country
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


private const val ITEM_VIEW_TYPE_HEADER = 0
private const val ITEM_VIEW_TYPE_ITEM = 1

class CountryListAdapter(private val onclickListener: OnclickLister) :
    ListAdapter<DataItem, RecyclerView.ViewHolder>(
        DataItemDiffCallback()
    ) {

    private val adapterScope = CoroutineScope(Dispatchers.Default)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder.from(parent)
            ITEM_VIEW_TYPE_ITEM -> CountryViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType ${viewType}")
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder){
            is CountryViewHolder -> {
                val countryItem = getItem(position) as DataItem.CountryItem
                holder.itemView.setOnClickListener { onclickListener.onclick(countryItem.country) }
                holder.bind(countryItem.country)
            }
            is HeaderViewHolder ->  {
                val header = getItem(position) as DataItem.Header
                holder.bind(header)
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)){
            is DataItem.Header -> ITEM_VIEW_TYPE_HEADER
            is DataItem.CountryItem -> ITEM_VIEW_TYPE_ITEM
        }
    }

    fun addHeadersAndSubmitList(countries: List<Country>?){
        adapterScope.launch {
            var items: MutableList<DataItem> = mutableListOf()
            countries?.let {
                var continent = ""


                for (country in countries){
                    if(continent != country.continentName){
                        continent = country.continentName
                        items.add(DataItem.Header(continent))

                    }
                    items.add(DataItem.CountryItem(country))
                }
            }

            withContext(Dispatchers.Main){
                submitList(items)
            }
        }
    }

    class CountryViewHolder private constructor(private val binding: ListItemCountryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(country: Country) {
            binding.country = country
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): CountryViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemCountryBinding.inflate(layoutInflater,parent,false)
                return CountryViewHolder(binding)
            }
        }
    }

    class HeaderViewHolder private constructor(private val binding: ListItemHeaderBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(header: DataItem.Header) {
            binding.headerItem = header
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): HeaderViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemHeaderBinding.inflate(layoutInflater,parent,false)

                return HeaderViewHolder(binding)
            }
        }
    }

    class OnclickLister(val clickListener: (country: Country) -> Unit) {
        fun onclick(country: Country) = clickListener(country)
    }
}

class DataItemDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }

}

sealed class DataItem {
    data class CountryItem(val country: Country): DataItem() {
        override val id = country.id
    }

    class Header(val continentName: String): DataItem() {
        override val id = Int.MIN_VALUE
    }

    abstract val id: Int
}

