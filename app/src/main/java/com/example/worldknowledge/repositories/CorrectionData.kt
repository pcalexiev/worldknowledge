package com.example.worldknowledge.repositories

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds

object CorrectionData {

    // south -> west ->
    // north -> east
    val correctBoundsForCountries = mapOf(
        Pair(
            "U.S. Minor Outlying Islands",
            LatLngBounds(
                LatLng(19.260943921426205, 166.58899711608884),
                LatLng(19.32858362263115, 166.66649361908436)
            )
        ),
        Pair(
            "Antarctica",
            LatLngBounds(
                LatLng(-80.9999, -179.9999),
                LatLng(-60.515533, 179.9999)
            )
        ),
        Pair(
            "Russia",
            LatLngBounds(
                LatLng(41.1853530000001, 19.25),
                LatLng(81.8616409300001, 169.05)
            )
        ),
        Pair(
            "Saint Helena",
            LatLngBounds(
                LatLng(-19.692620696516784, -15.794306471943857),
                LatLng(-4.208742906383882, -4.4784025847911835)
            )
        ),
        Pair(
            "Seychelles",
            LatLngBounds(
                LatLng(-14.095381477320197, 51.38505354523659),
                LatLng(0.47855584453832756, 61.365254223346696)
            )
        ),
        Pair(
            "São Tomé and Príncipe",
            LatLngBounds(
                LatLng(-0.095234, 6.37017),
                LatLng(1.801323, 7.566374)
            )
        ),
        Pair(
            "French Southern Territories",
            LatLngBounds(
                LatLng(-50.383369688842386, 68.50673764944077),
                LatLng(-48.15498280986118, 70.6873930245638)
            )
        ),
        Pair(
            "Heard Island and McDonald Islands",
            LatLngBounds(
                LatLng(-53.33840176342309, 73.2125985994935),
                LatLng(-52.800317050391904, 73.82161997258663)
            )
        ),
        Pair(
            "South Georgia and South Sandwich Islands",
            LatLngBounds(
                LatLng(-60.36319341, -38.9479509639999),
                LatLng(-53.080896636, -25.352069712)
            )
        ),
        Pair(
            "Cocos [Keeling] Islands",
            LatLngBounds(
                LatLng(-12.218725839, 96.806941408),
                LatLng(-12.082459094, 96.939489344)
            )
        ),
        Pair(
            "Malaysia",
            LatLngBounds(
                LatLng(0.6166604686231165, 100.04957903176546),
                LatLng(6.852887177389163, 104.31697860360146)
            )
        ),
        Pair(
            "Svalbard and Jan Mayen",
            LatLngBounds(
                LatLng(75.5522652630569, 9.830590076744556),
                LatLng(82.15797216524716, 33.83118711411953)
            )
        ),
        Pair(
            "Kiribati",
            LatLngBounds(
                LatLng(1.5390970714105112, -157.59504038840532),
                LatLng(2.228454219081105, -157.12424498051405)
            )
        )
    )

    val allCountriesForZoomCorrection: List<String> = listOf(
        "Canada",
        "Greenland",
        "Russia",
        "Antarctica"
    )

    val correctedCentersOfCountries = mapOf(
        Pair("Algeria", LatLng(28.769302264362345, 2.5641609728336334)),
        Pair("Angola", LatLng(-11.908441297771127, 17.512759566307068)),
        Pair("Benin", LatLng(9.830538296339038, 2.297254353761673)),
        Pair("Botswana", LatLng(-21.870380049546657, 23.792613185942177)),
        Pair("Burkina Faso", LatLng(12.685199716680644, -1.7233380675315857)),
        Pair("Burundi", LatLng(-3.263597610883247, 29.920063354074955)),
        Pair("Cameroon", LatLng(5.909130191794453, 12.688659466803074)),
        Pair("Central African Republic", LatLng(7.527617137710124, 20.513576455414295)),
        Pair("Congo Republic", LatLng(-0.35566902467328254, 15.164861418306828)),
        Pair("DR Congo", LatLng(-1.3300540268611873, 23.58058724552393)),
        Pair("Egypt", LatLng(27.30166741864446, 29.79272682219744)),
        Pair("Equatorial Guinea", LatLng(1.7114705374844739, 10.475673079490662)),
        Pair("Eritrea", LatLng(15.58923756289287, 38.80689136683941)),
        Pair("Ethiopia", LatLng(9.402043782629244, 39.63769152760506)),
        Pair("Gabon", LatLng(-0.21835710149852622, 11.769488230347632)),
        Pair("Gambia", LatLng(13.599934022677697, -15.336162708699705)),
        Pair("Ghana", LatLng(8.211042009023828, -1.224890761077404)),
        Pair("Guinea", LatLng(10.849032980865978, -10.926549397408962)),
        Pair("Guinea-Bissau", LatLng(12.166896674347703, -14.968856982886791)),
        Pair("Ivory Coast", LatLng(7.881792998106345, -5.581662878394128)),
        Pair("Kenya", LatLng(0.7925175351011176, 37.93034680187702)),
        Pair("Lesotho", LatLng(-29.49466207086945, 28.26054912060499)),
        Pair("Liberia", LatLng(6.525501803365796, -9.331183210015299)),
        Pair("Libya", LatLng(27.81485873205351, 17.443441562354565)),
        Pair("Madagascar", LatLng(-20.627112108738963, 46.10472679138184)),
        Pair("Malawi", LatLng(-13.362470243838514, 34.25004255026579)),
        Pair("Mali", LatLng(18.628916528390103, -1.8322830274701118)),
        Pair("Mauritania", LatLng(21.056417213010924, -10.414861366152765)),
        Pair("Morocco", LatLng(32.34586342900874, -6.352289691567421)),
        Pair("Mozambique", LatLng(-16.95997075532666, 35.52646458148956)),
        Pair("Namibia", LatLng(-21.559880368288976, 17.146705761551857)),
        Pair("Niger", LatLng(18.12081393375681, 9.370544962584972)),
        Pair("Nigeria", LatLng(10.087196603571359, 8.080829232931135)),
        Pair("Seychelles", LatLng(-3.569798477905131, 56.51419054716826)),
        Pair("Sierra Leone", LatLng(8.711848696634013, -11.786998026072977)),
        Pair("Somalia", LatLng(3.3149432337398803, 45.133314840495586)),
        Pair("South Africa", LatLng(-30.14760143283838, 24.2550028860569)),
        Pair("South Sudan", LatLng(8.196094366562145, 29.937694519758224)),
        Pair("Sudan", LatLng(17.151484834195543, 30.384190455079075)),
        Pair("Tanzania", LatLng(-5.940732571496638, 34.95375227183104)),
        Pair("Togo", LatLng(8.831156634380449, 1.0025108233094213)),
        Pair("Tunisia", LatLng(34.29706734868058, 9.565151631832123)),
        Pair("Uganda", LatLng(1.7789514240900122, 32.33231205493212)),
        Pair("Western Sahara", LatLng(25.042381991382527, -13.16029440611601)),
        Pair("Zimbabwe", LatLng(-18.700142355449156, 29.85587742179632)),
        Pair("Antarctica", LatLng(-74.94981835581517, 22.040859796106815)),
        Pair("Bouvet Island", LatLng(-54.41690223770222, 3.346274010837078)),
        Pair("French Southern Territories", LatLng(-49.17326532899941, 69.48917079716921)),
        Pair("Heard Island and McDonald Islands", LatLng(-53.05013552795215, 73.49899649620056)),
        Pair(
            "South Georgia and South Sandwich Islands",
            LatLng(-56.850346492641144, -31.08036383986473)
        ),
        Pair("Afghanistan", LatLng(34.322055322764605, 64.98366683721542)),
        Pair("Armenia", LatLng(40.388184150658525, 44.59812059998512)),
        Pair("Azerbaijan", LatLng(40.55113142093017, 48.008383736014366)),
        Pair("Bahrain", LatLng(26.05847289769606, 50.54402478039265)),
        Pair("Bangladesh", LatLng(24.43962608591101, 90.23900762200356)),
        Pair("Brunei", LatLng(4.552997965462541, 114.63672347366808)),
        Pair("Cambodia", LatLng(13.048653259434316, 104.93964437395334)),
        Pair("China", LatLng(37.00407510860914, 103.79521921277046)),
        Pair("Hong Kong", LatLng(22.356188774887606, 114.1684902459383)),
        Pair("Georgia", LatLng(42.352086726336616, 43.52066382765771)),
        Pair("India", LatLng(23.541259856055454, 79.57339357584715)),
        Pair("Indonesia", LatLng(-0.6272465238606757, 115.15940990298986)),
        Pair("Iran", LatLng(32.694969246437275, 54.25432685762644)),
        Pair("Iraq", LatLng(33.328065327636416, 42.984206937253475)),
        Pair("Israel", LatLng(31.124123228438318, 34.845000095665455)),
        Pair("Japan", LatLng(37.05931735615277, 138.84574960917234)),
        Pair("Jordan", LatLng(31.468168661400707, 36.79822124540806)),
        Pair("Kazakhstan", LatLng(49.442885254356604, 67.35675796866417)),
        Pair("Kuwait", LatLng(29.3815054688503, 47.40140184760094)),
        Pair("Kyrgyzstan", LatLng(41.756739985348545, 74.54509757459164)),
        Pair("Laos", LatLng(19.87512971888968, 102.46782515197992)),
        Pair("Lebanon", LatLng(34.165708137509945, 35.884754061698914)),
        Pair("Macao", LatLng(22.163586479966963, 113.56388311833143)),
        Pair("Malaysia", LatLng(3.9534848093723842, 102.02114079147577)),
        Pair("Mongolia", LatLng(47.67265270131319, 103.1461715698242)),
        Pair("Myanmar", LatLng(22.180459118649566, 96.45573209971188)),
        Pair("Nepal", LatLng(28.44424059780503, 83.93366698175669)),
        Pair("North Korea", LatLng(40.38372600167344, 127.16547299176455)),
        Pair("Oman", LatLng(20.8157976891876, 56.11461851745844)),
        Pair("Pakistan", LatLng(30.475923362651525, 69.42092031240463)),
        Pair("Palestine", LatLng(32.03014619121432, 35.27157124131917)),
        Pair("Philippines", LatLng(12.904191792794403, 122.99854159355165)),
        Pair("Qatar", LatLng(25.36539402047751, 51.19075298309326)),
        Pair("Singapore", LatLng(1.3780273568751182, 103.86762477457523)),
        Pair("Sri Lanka", LatLng(7.703874920881561, 80.6923146173358)),
        Pair("Syria", LatLng(35.204375874720824, 38.50377928465605)),
        Pair("Taiwan", LatLng(24.00878005003872, 121.07319261878729)),
        Pair("Tajikistan", LatLng(38.779520271405374, 71.03795137256384)),
        Pair("Thailand", LatLng(15.813295113176169, 101.0094541311264)),
        Pair("Turkey", LatLng(39.64632884412019, 35.40128856897354)),
        Pair("Turkmenistan", LatLng(39.467157705389056, 58.91577780246735)),
        Pair("United Arab Emirates", LatLng(24.0346721875828, 54.14093714207411)),
        Pair("Uzbekistan", LatLng(42.53303698946766, 63.92194859683513)),
        Pair("Vietnam", LatLng(14.823144214251217, 108.13126757740974)),
        Pair("Yemen", LatLng(16.19406712438326, 47.55355149507523)),
        Pair("Albania", LatLng(41.166664782911745, 20.056865997612476)),
        Pair("Andorra", LatLng(42.548478606255614, 1.5860025957226753)),
        Pair("Austria", LatLng(47.785215871266075, 14.13761392235756)),
        Pair("Belarus", LatLng(53.46779525779229, 28.030527904629707)),
        Pair("Belgium", LatLng(50.58007409036671, 4.653519652783871)),
        Pair("Bosnia and Herzegovina", LatLng(44.8107933876551, 17.79151551425457)),
        Pair("Bulgaria", LatLng(42.671203036897914, 25.21140150725842)),
        Pair("Croatia", LatLng(45.24434127520205, 14.800736792385576)),
        Pair("Cyprus", LatLng(35.1094188000015, 33.220170848071575)),
        Pair("Czechia", LatLng(49.892899802744694, 15.123816579580309)),
        Pair("Denmark", LatLng(55.75503805667508, 10.09268682450056)),
        Pair("Estonia", LatLng(58.86378710106664, 25.816174261271954)),
        Pair("Faroe Islands", LatLng(62.08593790831759, -6.703632250428201)),
        Pair("Finland", LatLng(62.24891677746958, 26.255040653049946)),
        Pair("France", LatLng(47.161016111814426, 2.4130939319729805)),
        Pair("Germany", LatLng(51.44102330921676, 10.428303591907023)),
        Pair("Gibraltar", LatLng(36.14222991811212, -5.353761613368988)),
        Pair("Greece", LatLng(39.71168385511186, 22.577674090862274)),
        Pair("Guernsey", LatLng(49.47020461875324, -2.5763217732310295)),
        Pair("Iceland", LatLng(65.18045275738825, -18.65624561905861)),
        Pair("Ireland", LatLng(53.2942397957953, -8.151567801833153)),
        Pair("Isle of Man", LatLng(54.23959069813256, -4.525964185595512)),
        Pair("Italy", LatLng(43.29699587096728, 12.441016137599945)),
        Pair("Jersey", LatLng(49.219346871173265, -2.1357763186097145)),
        Pair("Latvia", LatLng(57.18178534437831, 25.985454842448235)),
        Pair("Liechtenstein", LatLng(47.14211467807757, 9.556744918227198)),
        Pair("Lithuania", LatLng(55.760156922486374, 23.85560821741819)),
        Pair("Luxembourg", LatLng(49.65400367324991, 6.128857322037221)),
        Pair("Malta", LatLng(35.897742966362976, 14.446184970438479)),
        Pair("Moldova", LatLng(47.726494520869856, 28.43915306031704)),
        Pair("Monaco", LatLng(43.738308766185035, 7.4207496643066415)),
        Pair("Montenegro", LatLng(42.86540572166715, 19.325647205114365)),
        Pair("Netherlands", LatLng(52.30388207327734, 5.629572831094265)),
        Pair("Norway", LatLng(62.310199797179074, 9.460467360913754)),
        Pair("Poland", LatLng(53.291968642238835, 18.72568465769291)),
        Pair("Portugal", LatLng(39.80955363430326, -8.619527108967304)),
        Pair("Romania", LatLng(46.07026590361056, 24.93585146963596)),
        Pair("Russia", LatLng(63.44713435339084, 93.68442412465811)),
        Pair("San Marino", LatLng(43.93925453383836, 12.446628324687481)),
        Pair("Serbia", LatLng(44.23836842126366, 20.78699313104153)),
        Pair("Slovakia", LatLng(48.89827923029908, 19.35967907309532)),
        Pair("Slovenia", LatLng(46.16239175233677, 14.633394777774813)),
        Pair("Spain", LatLng(39.98794261351331, -3.2950247079133987)),
        Pair("Svalbard and Jan Mayen", LatLng(78.78501434116112, 17.004430331289768)),
        Pair("Sweden", LatLng(63.37812099132457, 16.797853633761406)),
        Pair("Switzerland", LatLng(46.91859550617607, 7.943427711725235)),
        Pair("Ukraine", LatLng(49.61396816566859, 31.278664581477646)),
        Pair("United Kingdom", LatLng(55.489153398622626, -2.750466540455818)),
        Pair("Vatican City", LatLng(41.90255852258412, 12.453624196350574)),
        Pair("Åland", LatLng(60.32751638028772, 20.100408978760242)),
        Pair("Anguilla", LatLng(18.221803608688006, -63.06079689413309)),
        Pair("Aruba", LatLng(12.514153957034623, -69.9726627767086)),
        Pair("Bahamas", LatLng(23.97723138688414, -76.67371977120638)),
        Pair("Barbados", LatLng(13.178273451723772, -59.54679194837808)),
        Pair("Belize", LatLng(17.10710053211653, -88.6112504079938)),
        Pair("Bermuda", LatLng(32.30058182313497, -64.76386316120625)),
        Pair("Bonaire, Sint Eustatius, and Saba", LatLng(12.210520549939927, -68.26330069452524)),
        Pair("Canada", LatLng(61.166917672058574, -112.03607071191072)),
        Pair("Costa Rica", LatLng(10.562820620209827, -84.17589828372002)),
        Pair("Cuba", LatLng(22.04026885510737, -78.98362912237644)),
        Pair("Curaçao", LatLng(12.181700180108091, -68.99144299328327)),
        Pair("Dominica", LatLng(15.503772177137323, -61.376280449330814)),
        Pair("Dominican Republic", LatLng(19.092314554087036, -70.20830187946558)),
        Pair("El Salvador", LatLng(13.824790162357699, -88.7280297651887)),
        Pair("Greenland", LatLng(66.42690673466535, -46.202093325555325)),
        Pair("Grenada", LatLng(12.12838908267539, -61.68828003108501)),
        Pair("Guadeloupe", LatLng(16.089757638653904, -61.27183254808187)),
        Pair("Guatemala", LatLng(15.863190017925236, -90.38398001343012)),
        Pair("Haiti", LatLng(19.052766792066986, -72.65607744455338)),
        Pair("Honduras", LatLng(15.180421081135895, -87.09653154015541)),
        Pair("Jamaica", LatLng(18.246761998746525, -77.31661174446344)),
        Pair("Martinique", LatLng(14.672562487601903, -61.02636583149433)),
        Pair("Mexico", LatLng(25.148680763078563, -102.66781602054834)),
        Pair("Nicaragua", LatLng(13.180123418587572, -85.05395628511906)),
        Pair("Panama", LatLng(8.794270340070646, -80.10456148535015)),
        Pair("Puerto Rico", LatLng(18.29306994898353, -66.66781615465881)),
        Pair("Saint Lucia", LatLng(13.858255683695251, -61.0043878108263)),
        Pair("Saint Martin", LatLng(18.075913634909263, -63.05084388703108)),
        Pair("Saint Pierre and Miquelon", LatLng(46.96900071182727, -56.329023241996765)),
        Pair("Sint Maarten", LatLng(18.043788870882857, -63.058984726667404)),
        Pair("Trinidad and Tobago", LatLng(10.478031592387644, -61.30383465439082)),
        Pair("Turks and Caicos Islands", LatLng(21.789690823526737, -71.74838103353979)),
        Pair("United States", LatLng(41.422642592535844, -101.62217695266008)),
        Pair("Australia", LatLng(-23.536442326524966, 134.070213586092)),
        Pair("Fiji", LatLng(-17.633481599154006, 177.96326793730256)),
        Pair("Guam", LatLng(13.45881872027826, 144.77474179118872)),
        Pair("Kiribati", LatLng(1.8664195717855525, -157.37622275948524)),
        Pair("New Caledonia", LatLng(-21.17548028362035, 165.4621636122465)),
        Pair("New Zealand", LatLng(-41.981691174509656, 172.27489639073607)),
        Pair("Niue", LatLng(-19.06596299769223, -169.85251430422065)),
        Pair("Norfolk Island", LatLng(-29.01946227885405, 167.93935898691416)),
        Pair("Papua New Guinea", LatLng(-5.886087236134482, 145.00128518790007)),
        Pair("Samoa", LatLng(-13.575834310694264, -172.43554931133986)),
        Pair("Solomon Islands", LatLng(-8.99254110051634, 160.0396777689457)),
        Pair("Timor-Leste", LatLng(-8.601935430590405, 125.91003958135843)),
        Pair("Argentina", LatLng(-34.29742188323718, -65.26497825980186)),
        Pair("Bolivia", LatLng(-16.25446744915158, -64.71238184720278)),
        Pair("Brazil", LatLng(-6.437092566588829, -55.886538214981556)),
        Pair("Chile", LatLng(-39.0916709888478, -72.52361990511417)),
        Pair("Colombia", LatLng(4.249252018180301, -73.07880330830812)),
        Pair("Ecuador", LatLng(-1.1815925873962634, -78.39220136404037)),
        Pair("Falkland Islands", LatLng(-51.60177695245465, -58.76332506537437)),
        Pair("French Guiana", LatLng(4.895168589612948, -52.82631777226925)),
        Pair("Guyana", LatLng(4.974780731365359, -58.97477801889182)),
        Pair("Paraguay", LatLng(-22.913251168133193, -58.405451327562325)),
        Pair("Peru", LatLng(-9.742888945720097, -75.40239829570056)),
        Pair("Suriname", LatLng(4.341025825761106, -55.903510898351676)),
        Pair("Uruguay", LatLng(-32.63256989780783, -56.00998621433973)),
        Pair("Venezuela", LatLng(7.842350876784553, -66.24066665768623))
    )

}