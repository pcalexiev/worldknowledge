package com.example.worldknowledge.repositories


import androidx.lifecycle.MutableLiveData
import com.example.worldknowledge.domain.Country
import com.example.worldknowledge.network.Network
import com.example.worldknowledge.network.asDomainModel
import kotlinx.coroutines.*
import timber.log.Timber


class CountriesRepository {

    private val repositoryJob = Job()
    private val coroutineScope = CoroutineScope(repositoryJob + Dispatchers.Main)

    val countries = MutableLiveData<List<Country>>()
    val selectedCountryCode = MutableLiveData<String>()

    init {
        refreshCountries()
    }

    fun refreshCountries() {
        coroutineScope.launch {
            var getPropertiesDeferred = Network.geoNamesService.getCountries()

            try {
                val listResult = getPropertiesDeferred.await().asDomainModel()
                countries.value = listResult
            } catch (e: Exception) {
                Timber.e(e)
                countries.value = ArrayList()
            }

            countries.value =
                countries.value?.sortedWith(compareBy({ it.continentName }, { it.countryName }))

            correctCountriesBoundaries()

        }
    }

    private fun correctCountriesBoundaries() {
        for (country in countries.value!!) {
            if (CorrectionData.correctBoundsForCountries.containsKey(country.countryName)) {
                val correctionData =
                    CorrectionData.correctBoundsForCountries.get(country.countryName)

                correctionData?.let {
                    country.east = correctionData.northeast.longitude
                    country.west = correctionData.southwest.longitude
                    country.north = correctionData.northeast.latitude
                    country.south = correctionData.southwest.latitude
                }

            }
        }
    }

    fun requestCountryCodeFromLocation(latitude: Double, longitude: Double) {
        coroutineScope.launch {
            var getPropertiesDeferred =
                Network.geoNamesService.getCountryCodeFromLocation(latitude, longitude)

            try {
                val countryCode = getPropertiesDeferred.await()
                selectedCountryCode.value = countryCode
            } catch (e: Exception) {
                Timber.e(e)
                selectedCountryCode.value = null
            }
        }
    }

}