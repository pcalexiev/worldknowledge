package com.example.worldknowledge.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.example.worldknowledge.R
import com.example.worldknowledge.Utils
import com.example.worldknowledge.domain.Country
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.country_info_window.view.*
import kotlinx.android.synthetic.main.info_window_message.view.*

class CustomInfoWindowAdapter(val context: Context, val parentView: View?) :
    GoogleMap.InfoWindowAdapter {

    var isExpanded: Boolean = false
    var infoWindow: View
    var country: Country? = null

    init {
        val parent = parentView?.findViewById<FrameLayout>(R.id.map)
        infoWindow =
            LayoutInflater.from(context)
                .inflate(R.layout.country_info_window, parent, false)
    }

    private fun renderInfoWindow() {
        val parent = parentView?.findViewById<FrameLayout>(R.id.map)

        if (country == null) {

            infoWindow =
                LayoutInflater.from(context)
                    .inflate(R.layout.info_window_message, parent, false)

            infoWindow.apply {
                text_message.text = context.getString(R.string.no_known_country_at_location)
                image_show_more.visibility = View.GONE
            }

        } else {

            if (!isExpanded) {

                infoWindow =
                    LayoutInflater.from(context)
                        .inflate(R.layout.info_window_message, parent, false)

                infoWindow.apply {
                    text_message.text = country!!.countryName
                    image_show_more.visibility = View.VISIBLE
                }

            } else {

                infoWindow =
                    LayoutInflater.from(context)
                        .inflate(R.layout.country_info_window, parent, false)

                infoWindow.apply {
                    text_country_name.text = country!!.countryName
                    text_country_capital.text = country!!.capital
                    text_country_area.text = Utils.formatArea(country!!.areaInSqKm)
                    text_country_continent.text = country!!.continentName
                    text_country_languages.text =
                        Utils.formatLanguages(country!!.languages)
                    text_country_currency.text = country!!.currencyCode
                    text_country_population.text = Utils.formatPopulation(country!!.population)
                }
            }
        }
    }

    override fun getInfoContents(p0: Marker?): View {
        renderInfoWindow()
        return infoWindow
    }

    override fun getInfoWindow(p0: Marker?): View? {
        return null
    }

}