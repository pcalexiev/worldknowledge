package com.example.worldknowledge.ui

import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.worldknowledge.R

class WebViewFragment : Fragment() {

    lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_web_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView= view.findViewById(R.id.webview)
        webView.settings.javaScriptEnabled = true

        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                return false
            }
        }

        webView.setOnKeyListener(View.OnKeyListener { innerView, keyCode, keyEvent ->
            if(keyEvent.action == KeyEvent.ACTION_DOWN){
                webView = innerView as WebView

                if(keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()){
                    webView.goBack()
                    return@OnKeyListener true
                }
            }
            return@OnKeyListener false
        })

        val args = WebViewFragmentArgs.fromBundle(requireArguments())

        webView.loadUrl("https://en.wikipedia.org/wiki/" + args.countryName)
    }
}