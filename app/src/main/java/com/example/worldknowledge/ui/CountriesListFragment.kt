package com.example.worldknowledge.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.worldknowledge.R
import com.example.worldknowledge.databinding.FragmentCountriesListBinding
import com.example.worldknowledge.viewmodels.CountriesListViewModel
import com.example.worldknowledge.viewmodels.adapters.CountryListAdapter
import dagger.hilt.android.AndroidEntryPoint



@AndroidEntryPoint
class CountriesListFragment : Fragment() {

    private lateinit var binding: FragmentCountriesListBinding

    private val viewModel: CountriesListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_countries_list, container, false)

        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel
        binding.itemList.adapter = CountryListAdapter(CountryListAdapter.OnclickLister {
            viewModel.displayCountry(it)
        })

        viewModel.navigateToSelectedCountry.observe(viewLifecycleOwner, Observer { country ->
            country?.let {
                this.findNavController()
                    .navigate(
                        CountriesListFragmentDirections
                            .actionCountriesListFragmentToMapsFragment(country)
                    )
                viewModel.displayCountryComplete()
            }
        })

        return binding.root
    }
}