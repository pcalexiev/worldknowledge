package com.example.worldknowledge.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.worldknowledge.R
import com.example.worldknowledge.databinding.FragmentTitleBinding


class TitleFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentTitleBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_title,container,false)

        binding.buttonCountryList.setOnClickListener {
            this.findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToCountriesListFragment2())
        }

        binding.buttonGoogleMaps.setOnClickListener {
            this.findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToMapsFragment())
        }

        return binding.root
    }
}