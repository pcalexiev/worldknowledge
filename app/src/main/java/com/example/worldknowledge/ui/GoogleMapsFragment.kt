package com.example.worldknowledge.ui


import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.worldknowledge.R
import com.example.worldknowledge.domain.Country
import com.example.worldknowledge.repositories.CorrectionData
import com.example.worldknowledge.viewmodels.GoogleMapsViewModel
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class GoogleMapsFragment : Fragment() {

    private val viewModel: GoogleMapsViewModel by viewModels()
    private var marker: Marker? = null
    private lateinit var infoWindowAdapter: CustomInfoWindowAdapter

    private val callback = OnMapReadyCallback { googleMap ->

        showDefaultPosition()


        googleMap.setInfoWindowAdapter(infoWindowAdapter)

        val args = GoogleMapsFragmentArgs.fromBundle(requireArguments())
        args.selectedCountry?.let { country ->
            googleMap.clear()
            marker =
                googleMap.addMarker(MarkerOptions().position(viewModel.findCenterOfCountry(country)))
            viewModel.updateCountry(country)

            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngBounds(
                    viewModel.createCameraBoundsForCountry(
                        country
                    ), 0
                )
            )

            checkIfCountryHasToBeZoomed(country, googleMap)

            infoWindowAdapter.isExpanded = false
            infoWindowAdapter.country = country

            marker?.showInfoWindow()
        }

        googleMap?.setOnMapClickListener { position ->
            googleMap.clear()
            marker = googleMap.addMarker(MarkerOptions().position(position))
            viewModel.requestCountryFromLocationCalled = true
            viewModel.requestCountryFromLocation(position)

        }

        viewModel.selectedCountryCode.observe(viewLifecycleOwner, Observer {

            if (viewModel.requestCountryFromLocationCalled) {
                viewModel.requestCountryFromLocationCalled = false

                viewModel.findCountryFromCountryCode()

                infoWindowAdapter.isExpanded = false
                infoWindowAdapter.country = viewModel.selectedCountry.value

                viewModel.selectedCountry.value?.let { country ->

                    googleMap.animateCamera(
                        CameraUpdateFactory.newLatLngBounds(
                            viewModel.createCameraBoundsForCountry(country), 0
                        )
                    )

                    checkIfCountryHasToBeZoomed(country, googleMap)
                }

                marker?.showInfoWindow()
            }

        })

        googleMap.setOnInfoWindowClickListener { marker ->

            val country = viewModel.selectedCountry.value

            country?.let {

                if (infoWindowAdapter.isExpanded) {
                    infoWindowAdapter.isExpanded = false

                    googleMap.animateCamera(
                        CameraUpdateFactory.newLatLngBounds(
                            viewModel.createCameraBoundsForCountry(country), 0
                        )
                    )

                } else {
                    infoWindowAdapter.isExpanded = true

                    val screenSouthLatitude =
                        googleMap.projection.visibleRegion.latLngBounds.southwest.latitude
                    val screenNorthLatitude =
                        googleMap.projection.visibleRegion.latLngBounds.northeast.latitude

                    val correctedLatitude =
                        marker.position.latitude + ((screenNorthLatitude - screenSouthLatitude) / 3)

                    val correctedCameraPosition =
                        LatLng(correctedLatitude, marker.position.longitude)

                    googleMap.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            correctedCameraPosition,
                            googleMap.cameraPosition.zoom
                        )
                    )

                }
            }

            marker.showInfoWindow()
        }

        googleMap.setOnMarkerClickListener { marker ->
            googleMap.clear()
            viewModel.updateCountry(null)
            return@setOnMarkerClickListener true
        }
    }

    private fun checkIfCountryHasToBeZoomed(
        country: Country,
        googleMap: GoogleMap
    ) {
        if (CorrectionData.allCountriesForZoomCorrection.contains(country.countryName)) {

            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    marker?.position,
                    viewModel.correctZoom(
                        marker!!.position.latitude,
                        googleMap.cameraPosition.zoom
                    )
                )
            )
        }
    }

    private fun showDefaultPosition() {
        viewModel.requestCountryFromLocation(LatLng(0.0, 0.0))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)

        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.overflow_menu, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.wikipedia_link -> openWikipediaUrl()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openWikipediaUrl() {
        if (viewModel.selectedCountry.value != null) {
            this.findNavController().navigate(
                GoogleMapsFragmentDirections.actionMapsFragmentToWebViewFragment(
                    viewModel.selectedCountry.value!!.countryName
                )
            )
        } else {
            val snackbar = Snackbar.make(
                requireView(),
                R.string.no_valid_country_selected,
                Snackbar.LENGTH_SHORT
            )
            val snackbarView = snackbar.view
            snackbarView.setBackgroundColor(resources.getColor(R.color.primaryTextColor))
            snackbar.show()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        infoWindowAdapter = CustomInfoWindowAdapter(requireContext(), view)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }
}